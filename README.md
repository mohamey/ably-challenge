# Ably Protocol Design Challenge

## Protocol Design
The core principle in my design that the client is responsible for ensuring it's correctly received all parts (or segments) of an object. As such, I've implemented four key actions in this protocol:

* connect - This action is responsible for initialising a new Session on the server given a clientId. We also optionally include the number of segments we want in the response (this correlates to the amount of numbers we generate on the server). The Server responds with teh number of segments it will stream to the client in the response, and the client's sessionId. One client can have multiple sessions.
* stream - This action is used by the client to indicate to the server that the client is ready to begin receiving segments. The server begins streaming segments back to the client until completion. Flow is controlled by the server. When the server sends the last segment, it includes the checksum for the entire object with it.
* reconnect - If the connection drops for whatever reason, it is the responsibility of the client to reconnect before the session is closed on the server. When reconnecting, the client passes the index of the last segment it received and the server responds with the sessionId & number of segments left to stream - or it returns a `NOT_FOUND` error because the session is no longer available.
* closeSession - This isn't necessary as the server will clean up an inactive session anyway. But if the client would like to be polite, it can call on the server to close it's session given a sessionId.

## Protocol Implementation
Implemented using gRPC. You can see the `.proto` file in `src/main/proto/MessageService.proto`. You can also see the `MessageService` which defines the above protocol.

## Assumptions

* Since we're using gRPC, we can expect messages to be delivered in order
* Clients can disconnect multiple times


## Solution

### Requirements
Implemented using:

* Java 17
* SpringBoot 2.7.5
* gRPC syntax version 3
* maven 3.8.6


### Run Instructions

Ensure you have the above requirements install. 

To run the server:
```shell
mvn spring-boot:run -Dspring-boot.run.arguments="--runType=server --port=8081"
```

To execute various use-cases in the client, use the following commands
```shell
# Test the happy path where the stream doesn't break
mvn spring-boot:run -Dspring-boot.run.arguments="--runType=client --action=happyPath --port=8081 --numSegments=5"

# Test the case where the stream breaks and the client reconnects
mvn spring-boot:run -Dspring-boot.run.arguments="--runType=client --action=breakAndResume --port=8081 --numSegments=5 --breakIndex=2"

# Test the case where the stream breaks and waits before attempting to reconnect. Note - sessions won't timeout exactly 30 seconds of being idle. Might have to wait longer than 30 to allow session to expire
mvn spring-boot:run -Dspring-boot.run.arguments="--runType=client --action=breakAndResumeAfterTimeout --port=8081 --numSegments=5 --breakIndex=2 --timeout=50"

# Test the case where the client drops the connection half way through
mvn spring-boot:run -Dspring-boot.run.arguments="--runType=client --action=breakAndDontResume --port=8081 --numSegments=5 --breakIndex=2"
```

Client Arguments:

* runType - client/server
* action - `happyPath`/`breakAndResume`/`breakAndResumeAfterTimeout`/`breakAndDontResume`. Define's client behaviour
* port - Required. port the server is running on 
* numSegments - Optional. The number of random numbers to generate. Optional.
* breakIndex - Mandatory if action is not `happyPath`. Indicates at which segment we should break the stream.
* timeout - Mandatory for action `breakAndResumeAfterTimeout`. Specifies how many seconds client waits before reconnecting to the stream.

**Note - you'll have to manually kill the client process. Wasn't able to figure out how to gracefully terminate a gRPC connection**.

#### Sample Output
The below is a snippet indicating how to compare the checksums generated by the solution

```log
Received Segment 5 - Payload: 1165867328
Received checksum 7115811 # <-- Checksum received from server
Finished!
Checksum is 7115811 # <-- Checksum we generated
Closing session 2b9f31c0-ce7e-44ef-a365-6a5d8e449911
```

### Implementation Problems

* When the client breaks the stream, it does not correctly close the channel. Even though we establish a new channel and continue streaming data, the initial channel remains open and prevents the client program exiting.

### Run Tests

```shell
mvn test
```

## Implementation Notes

### Terminology
* Transmittable Object - Any object which can be sent by this service. A transmittable object consists of multiple segments. The `RandomNumberGenerator` class which generates the numbers for this challenge is a Transmittable Object
* Segment - Part of a transmittable object. For example, a random number generated by the `RandomNumberGenerator` class is a segment.

### Session Management
* Each client can have multiple sessions.
* The `InMemorySessionService` will automatically remove any sessions that haven't been interacted with in over 30 seconds
* Session's last interaction time is refreshed every time a segment is sent for the session.

## Scenarios
### Happy Path - No Connection Drops
1. Client executes `connect` RPC with clientId to create a new session. Server creates new session for the client and returns the sessionId to the client, along with the number of segments the server will send.
2. Client executes `stream` RPC with the sessionId and the index of the first segment to stream from. Since this is the first time calling the `stream` command, it's called with index 1
3. The server streams all the segments (random numbers) back to the client at one second intervals. When sending the final segment, the server includes a checksum in the message.
4. The client receives each of the segments and buffers them, and prints the checksum from the server. 
5. The client calculates the checksum for the series of segments it received and prints it
6. The client calls the `closeSession` RPC to terminate it's session

### Client Must Reconnect part-way through stream
1. Client executes `connect` RPC with clientId to create a new session. Server creates new session for the client and returns the sessionId to the client, along with the number of segments the server will send.
2. Client executes `stream` RPC with the sessionId and the index of the first segment to stream from. Since this is the first time calling the `stream` command, it's called with index 1
3. Part way through the stream (depending on CLI arguments), the client closes the channel.
4. Client executes the `reconnect` RPC to the server, with it's sessionId and the index of the last segment it received. If the session still exists, the client is okay to proceed to the next step. If the session doesn't exist, the client gets a `NOT_FOUND` error from the server
5. Client executes the `stream` RPC again with it's sessionId and the index of the segment it wants to resume from
6. The client receives each of the segments and buffers them, and prints the checksum from the server.
7. The client calculates the checksum for the series of segments it received and prints it
8. The client calls the `closeSession` RPC to terminate it's session

In this scenario, after step 3, the server stops sending segments to the now closed channel and the session will be deleted after 30 seconds.

### Client drops and never reconnects
1. Client executes `connect` RPC with clientId to create a new session. Server creates new session for the client and returns the sessionId to the client, along with the number of segments the server will send.
2. Client executes `stream` RPC with the sessionId and the index of the first segment to stream from. Since this is the first time calling the `stream` command, it's called with index 1
3. Part way through the stream (depending on CLI arguments), the client closes the channel and does not attempt to reconnect
4. After 30 seconds, the session is deleted from the server.
5. The client must create a new session.
