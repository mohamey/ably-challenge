package com.yasirmohamed.ably.components;

import com.yasirmohamed.ably.models.transmittable.RandomNumberGenerator;
import com.yasirmohamed.ably.proto.Segment;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


class IntegerSegmentProducerTest {
    private RandomNumberGenerator randomNumberGenerator;
    private StreamObserver<Segment> responseStream;

    @BeforeEach
    void setup() {
        this.randomNumberGenerator = Mockito.mock(RandomNumberGenerator.class);
        this.responseStream = Mockito.mock(StreamObserver.class);
    }

    @Test
    void testExecutingTaskSendsSegment() {
        var segmentIndex = 1;
        var producer = new IntegerSegmentProducer(randomNumberGenerator, responseStream, "someSession", segmentIndex);

        Mockito.when(randomNumberGenerator.getNumberOfSegments()).thenReturn(10);
        Mockito.when(randomNumberGenerator.getSegment(segmentIndex)).thenReturn(5);
        producer.getSendSegmentTask().run();

        Mockito.verify(randomNumberGenerator, Mockito.times(1)).getSegment(segmentIndex);
        Mockito.verify(responseStream, Mockito.times(1)).onNext(Mockito.any());
    }

    @Test
    void testExecutingTaskOnFinalSegmentClosesStreamWithChecksum() {
        var segmentIndex = 1;
        var producer = new IntegerSegmentProducer(randomNumberGenerator, responseStream, "someSession", segmentIndex);

        Mockito.when(randomNumberGenerator.getNumberOfSegments()).thenReturn(1);
        Mockito.when(randomNumberGenerator.getSegment(segmentIndex)).thenReturn(5);
        Mockito.when(randomNumberGenerator.getChecksum()).thenReturn(1234L);
        producer.getSendSegmentTask().run();

        Mockito.verify(randomNumberGenerator, Mockito.times(1)).getSegment(segmentIndex);
        Mockito.verify(randomNumberGenerator, Mockito.times(1)).getChecksum();
        Mockito.verify(responseStream, Mockito.times(1)).onNext(Mockito.any());
        Mockito.verify(responseStream, Mockito.times(1)).onCompleted();
    }

    @Test
    void testExecutingTaskTwiceIteratesSegments() {
        var segmentIndex = 1;
        var producer = new IntegerSegmentProducer(randomNumberGenerator, responseStream, "someSession", segmentIndex);

        Mockito.when(randomNumberGenerator.getNumberOfSegments()).thenReturn(3);
        Mockito.when(randomNumberGenerator.getSegment(segmentIndex)).thenReturn(5);
        Mockito.when(randomNumberGenerator.getSegment(segmentIndex + 1)).thenReturn(6);
        producer.getSendSegmentTask().run();
        producer.getSendSegmentTask().run();

        Mockito.verify(randomNumberGenerator, Mockito.times(1)).getSegment(segmentIndex);
        Mockito.verify(randomNumberGenerator, Mockito.times(1)).getSegment(segmentIndex + 1);
        Mockito.verify(responseStream, Mockito.times(2)).onNext(Mockito.any());
    }

    @Test
    void testExecutingTaskAfterCompletionDoesNothing() {
        var segmentIndex = 1;
        var producer = new IntegerSegmentProducer(randomNumberGenerator, responseStream, "someSession", segmentIndex);

        Mockito.when(randomNumberGenerator.getNumberOfSegments()).thenReturn(1);
        Mockito.when(randomNumberGenerator.getSegment(segmentIndex)).thenReturn(5);
        producer.getSendSegmentTask().run();
        producer.getSendSegmentTask().run();

        Mockito.verify(randomNumberGenerator, Mockito.times(1)).getSegment(segmentIndex);
        Mockito.verify(responseStream, Mockito.times(1)).onNext(Mockito.any());

    }

    @Test
    // TODO
    void testExecutingTaskAfterErrorDoesNothing() {
        var segmentIndex = 1;
        var producer = new IntegerSegmentProducer(randomNumberGenerator, responseStream, "someSession", segmentIndex);

        Mockito.when(randomNumberGenerator.getNumberOfSegments()).thenReturn(1);
        Mockito.when(randomNumberGenerator.getSegment(segmentIndex)).thenReturn(5);
//        Mockito.when(responseStream.onCompleted()).thenThrow(() -> new Exception("Oh no"));
        producer.getSendSegmentTask().run();
        producer.getSendSegmentTask().run();

        Mockito.verify(randomNumberGenerator, Mockito.times(1)).getSegment(segmentIndex);
        Mockito.verify(responseStream, Mockito.times(1)).onNext(Mockito.any());
    }
}
