package com.yasirmohamed.ably.models.transmittable;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.zip.CRC32C;

class RandomNumberGeneratorTest {
    @Test
    void testRandomNumberGeneratorGeneratesCorrectNumberOfSegments() {
        var numberOfSegments = 10;
        var rng = new RandomNumberGenerator(numberOfSegments);
        List<Integer> randomNumbers = Lists.newArrayList();

        for (int i = 0; i < numberOfSegments; i++) {
            var segmentIndex = i + 1;
            randomNumbers.add(rng.getSegment(segmentIndex));
        }

        Assertions.assertEquals(numberOfSegments, randomNumbers.size());
    }

    @Test
    void testRngReturnsCorrectChecksum() {
        var numberOfSegments = 10;
        var rng = new RandomNumberGenerator(numberOfSegments);
        var byteArray = new byte[numberOfSegments];

        for (int i = 0; i < numberOfSegments; i++) {
            var segmentIndex = i + 1;
            byteArray[i] = rng.getSegment(segmentIndex).byteValue();
        }

        var crc32 = new CRC32C();
        crc32.update(byteArray);
        var expectedChecksum = crc32.getValue();

        Assertions.assertEquals(expectedChecksum, rng.getChecksum());

    }
}
