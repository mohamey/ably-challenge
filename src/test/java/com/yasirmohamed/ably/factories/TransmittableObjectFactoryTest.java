package com.yasirmohamed.ably.factories;

import com.yasirmohamed.ably.exceptions.NoSuchObjectError;
import com.yasirmohamed.ably.services.sessions.SessionService;
import com.yasirmohamed.ably.services.scheduling.SchedulingService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class TransmittableObjectFactoryTest {
    private SessionService sessionService;
    private SchedulingService schedulingService;

    @BeforeEach
    void setup() {
        this.sessionService = Mockito.mock(SessionService.class);
        this.schedulingService = Mockito.mock(SchedulingService.class);
    }

    @Test
    void testGetNewRandomGeneratorCreatesNewInstanceInCache() throws NoSuchObjectError {
        var factory = new TransmittableObjectFactory(sessionService, schedulingService);
        var rng = factory.getNewRng("someSessionId", 10);
        var cachedRng = factory.getTransmittableObject("someSessionId");

        Assertions.assertEquals(rng, cachedRng);
    }

    @Test
    void testObjectsForInactiveSessionsAreRemovedFromCache() {
        var sessionId = "someSessionId";
        Mockito.when(sessionService.containsSession(sessionId)).thenReturn(false);
        var factory = new TransmittableObjectFactory(sessionService, schedulingService);
        factory.getNewRng(sessionId, 10);
        factory.deleteExpiredEntries();

        Assertions.assertThrows(NoSuchObjectError.class, () -> factory.getTransmittableObject(sessionId));
    }
}
