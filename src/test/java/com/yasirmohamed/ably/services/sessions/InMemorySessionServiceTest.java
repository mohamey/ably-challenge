package com.yasirmohamed.ably.services.sessions;

import com.yasirmohamed.ably.exceptions.NoSuchSessionError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class InMemorySessionServiceTest {
    @Test
    void testCreatingAndGettingNewSession() throws NoSuchSessionError {
        var sessionService = new InMemorySessionService();
        var clientId = "someClientId";

        var session = sessionService.createSession(clientId);
        var sessionId = session.getSessionId();
        var retrievedSession = sessionService.getSession(sessionId);

        Assertions.assertEquals(session, retrievedSession);
    }

    @Test
    void testGettingInvalidSessionThrowsException() {
        var sessionService = new InMemorySessionService();
        Assertions.assertThrows(NoSuchSessionError.class, () -> sessionService.getSession("fake session id"));
    }

    @Test
    void testCloseSessionMarksUnderlyingSessionInactive() throws NoSuchSessionError {
        var sessionService = new InMemorySessionService();
        var clientId = "someClientId";

        var session = sessionService.createSession(clientId);
        var sessionId = session.getSessionId();
        sessionService.removeSession(sessionId);

        Assertions.assertThrows(NoSuchSessionError.class, () -> sessionService.getSession(sessionId));
    }

    @Test
    void testRefreshSessionUpdatesSessionLastAccessedTime() throws NoSuchSessionError {
        var sessionService = new InMemorySessionService();
        var clientId = "someClientId";

        var session = sessionService.createSession(clientId);
        var sessionId = session.getSessionId();
        var initialLastAccessedTime = session.getLastRefreshedTime();
        sessionService.refreshSession(sessionId);

        Assertions.assertNotEquals(initialLastAccessedTime, session.getLastRefreshedTime());
    }

    @Test
    void testContainsSessionReturnsTrueWhenSessionExists() {
        var sessionService = new InMemorySessionService();
        var clientId = "someClientId";

        var session = sessionService.createSession(clientId);
        var sessionId = session.getSessionId();

        Assertions.assertTrue(sessionService.containsSession(sessionId));
    }

    @Test
    void testContainsSessionReturnsFalseWhenSessionDoesntExist() {
        var sessionService = new InMemorySessionService();
        var clientId = "someClientId";

        sessionService.createSession(clientId);

        Assertions.assertFalse(sessionService.containsSession("Some fake session"));
    }

    @Test
    void testKillExpiredSessionsSkipsValidSessions() {
        var sessionService = new InMemorySessionService();
        var clientId = "someClientId";

        var session = sessionService.createSession(clientId);
        sessionService.killExpiredSessions();

        Assertions.assertTrue(sessionService.containsSession(session.getSessionId()));
    }

    @Test
    void testKillExpiredSessionsKillsExpiredSessions() throws InterruptedException {
        var sessionService = new InMemorySessionService();
        var clientId = "someClientId";

        var session = sessionService.createSession(clientId);
        Thread.sleep(2000);
        sessionService.killExpiredSessions(1);

        Assertions.assertFalse(sessionService.containsSession(session.getSessionId()));
    }
}
