package com.yasirmohamed.ably.services.scheduling;

import com.yasirmohamed.ably.components.IntegerSegmentProducer;
import com.yasirmohamed.ably.services.sessions.InMemorySessionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class SchedulingServiceTest {
    private ScheduledExecutorService scheduledExecutorService;
    private InMemorySessionService sessionService;

    @BeforeEach
    void setup() {
        this.sessionService = Mockito.mock(InMemorySessionService.class);
        this.scheduledExecutorService = Mockito.mock(ScheduledExecutorService.class);
    }

    @Test
    void testCreatingSchedulingServiceSchedulesCleanupTasks() {
        new SchedulingService(sessionService, scheduledExecutorService);
        Mockito.verify(scheduledExecutorService, Mockito.times(2)).scheduleAtFixedRate(Mockito.any(),
                Mockito.eq(10L), Mockito.eq(10L), Mockito.eq(TimeUnit.SECONDS));
    }

    @Test
    void testScheduleNewSegmentProducerTaskSchedulesTaskEverySecond() {
        var schedulingService = new SchedulingService(sessionService, scheduledExecutorService);
        var producer = Mockito.mock(IntegerSegmentProducer.class);
        Mockito.when(producer.getSendSegmentTask()).thenReturn(null);
        schedulingService.scheduleNewSegmentProducerTask("Some Session", producer);

        Mockito.verify(scheduledExecutorService, Mockito.times(1)).scheduleAtFixedRate(Mockito.any(),
                Mockito.eq(0L), Mockito.eq(1L), Mockito.eq(TimeUnit.SECONDS));
    }

    @Test
    void testScheduleNewMaintenanceTaskSchedulesTaskEvery10Seconds() {
        var schedulingService = new SchedulingService(sessionService, scheduledExecutorService);
        schedulingService.scheduleNewMaintenanceTask(Mockito.mock(Runnable.class));

        Mockito.verify(scheduledExecutorService, Mockito.times(3)).scheduleAtFixedRate(Mockito.any(),
                Mockito.eq(10L), Mockito.eq(10L), Mockito.eq(TimeUnit.SECONDS));
    }

}
