package com.yasirmohamed.ably.models.transmittable;

/**
 * Interface which defines the functionality of any object which needs to be transmitted. Currently it's only a
 * Random Number Generator, but could in future be files on the Filesystem etc.
 */
public interface TransmittableObject<T> {
    int getNumberOfSegments();
    T getSegment(int i);
    long getChecksum();
}
