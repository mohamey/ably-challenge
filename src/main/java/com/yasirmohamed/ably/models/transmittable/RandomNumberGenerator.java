package com.yasirmohamed.ably.models.transmittable;

import java.util.Random;
import java.util.zip.CRC32C;
import java.util.zip.Checksum;

/**
 * Class responsible for generating Random Numbers for this task. My preference is to generate the numbers lazily,
 * so that's how this class is constructed. It has the potential to use considerable memory if the numberOfSegments == 0xffff.
 * Didn't have the time to write the data to disk.
 *
 */
public class RandomNumberGenerator implements TransmittableObject<Integer> {
    private final Integer[] segments;
    private final int numberOfSegments;
    private final Random rng = new Random();
    private static final int UPPER_BOUND = Integer.MAX_VALUE;

    public RandomNumberGenerator(int numberOfSegments) {
        this.segments = new Integer[numberOfSegments];
        this.numberOfSegments = numberOfSegments;
    }

    @Override
    public int getNumberOfSegments() {
        return this.numberOfSegments;
    }

    @Override
    public Integer getSegment(int segmentIndex) {
        // Arrays are zero indexed, segments are 1-indexed
        var arrayIndex = segmentIndex - 1;
        if (this.segments[arrayIndex] == null) {
            segments[arrayIndex] = this.rng.nextInt(UPPER_BOUND);
        }
        return this.segments[arrayIndex];
    }

    @Override
    public long getChecksum() {
        Checksum crc32 = new CRC32C();
        var byteArray = new byte[this.numberOfSegments];
        for (int i = 0; i < numberOfSegments; i++) {
            byteArray[i] = segments[i].byteValue();
        }

        crc32.update(byteArray);
        return crc32.getValue();
    }
}
