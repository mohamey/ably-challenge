package com.yasirmohamed.ably.models;


import lombok.Getter;

import java.time.Instant;

@Getter
public class Session {
    private final String sessionId;
    private final String clientId;
    private Instant lastRefreshedTime = Instant.now();

    public Session(String sessionId, String clientId) {
        this.sessionId = sessionId;
        this.clientId = clientId;
    }

    public void refresh() {
        this.lastRefreshedTime = Instant.now();
    }
}
