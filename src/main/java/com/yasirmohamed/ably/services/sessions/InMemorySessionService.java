package com.yasirmohamed.ably.services.sessions;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.yasirmohamed.ably.exceptions.NoSuchSessionError;
import com.yasirmohamed.ably.models.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.UUID;

/**
 * Basic in memory implementation of a Session Service. It only handles creating, getting, and closing sessions. It
 * also exposes the method killExpiredSessions which removes any sessions that haven't been utilised in over 30 seconds.
 * This is scheduled in the {@link com.yasirmohamed.ably.services.scheduling.SchedulingService}
 */
@Slf4j
@Service
public class InMemorySessionService implements SessionService {
    private final Map<String, Session> sessionStore;

    public InMemorySessionService() {
        this.sessionStore = Maps.newHashMap();
    }

    /**
     * Given a ClientID, generate a new session
     */
    @Override
    public Session createSession(String clientId) {
        var sessionId = UUID.randomUUID().toString();
        var session = new Session(sessionId, clientId);
        this.sessionStore.put(sessionId, session);
        return session;
    }

    /**
     * Given a session ID, get the relevant Session if it exists
     * @throws NoSuchSessionError If the session doesn't exist.
     */
    @Override
    public Session getSession(String sessionId) throws NoSuchSessionError {
        if (!sessionStore.containsKey(sessionId)) {
            throw new NoSuchSessionError();
        }
        return this.sessionStore.get(sessionId);
    }

    /**
     * Remove session from the cache given the sessionId
     * @throws NoSuchSessionError If the session doesn't exist
     */
    @Override
    public void removeSession(String sessionId) throws NoSuchSessionError {
        log.info("Removing session {}", sessionId);
        this.getSession(sessionId); // Throw error if session doesn't exist
        this.sessionStore.remove(sessionId);
    }

    /**
     * Check if session exists given session ID
     */
    @Override
    public boolean containsSession(String sessionId) {
        try {
            this.getSession(sessionId);
            return true;
        } catch (NoSuchSessionError e) {
            return false;
        }
    }

    /**
     * Refresh a session. Used to indicate a session is still in active use.
     */
    @Override
    public void refreshSession(String sessionId) throws NoSuchSessionError {
        var session = this.getSession(sessionId);
        session.refresh();
    }

    public void killExpiredSessions() {
        killExpiredSessions(30);
    }

    /**
     * Kill sessions that haven't been used in longer than N seconds.
     */
    public void killExpiredSessions(int seconds) {
        var expiryTime = Instant.now().minus(seconds, ChronoUnit.SECONDS);
        var keysToRemove = Lists.newArrayList();

        this.sessionStore.forEach((sessionId, session) -> {
            if (session.getLastRefreshedTime().isBefore(expiryTime)) {
                log.info("Killing session {}", sessionId);
                keysToRemove.add(sessionId);
            }
        });

        keysToRemove.forEach(this.sessionStore::remove);
    }
}