package com.yasirmohamed.ably.services.sessions;

import com.yasirmohamed.ably.exceptions.NoSuchSessionError;
import com.yasirmohamed.ably.models.Session;

public interface SessionService {
    Session createSession(String clientId);
    Session getSession(String sessionId) throws NoSuchSessionError;
    void removeSession(String sessionId) throws NoSuchSessionError;
    void refreshSession(String sessionId) throws NoSuchSessionError;
    boolean containsSession(String sessionId);
}
