package com.yasirmohamed.ably.services.messaging;

import com.yasirmohamed.ably.components.IntegerSegmentProducer;
import com.yasirmohamed.ably.exceptions.NoSuchObjectError;
import com.yasirmohamed.ably.exceptions.NoSuchSessionError;
import com.yasirmohamed.ably.exceptions.SegmentOutOfBoundsError;
import com.yasirmohamed.ably.models.transmittable.TransmittableObject;
import com.yasirmohamed.ably.proto.*;
import com.yasirmohamed.ably.services.scheduling.SchedulingService;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.yasirmohamed.ably.factories.TransmittableObjectFactory;
import com.yasirmohamed.ably.services.sessions.SessionService;

import java.util.Random;

/**
 * GRPC Service implementation that is responsible for handing client communication via our protocol. It sends segments
 * back to the client at one second intervals by scheduling a task to run every second using an external threadpool.
 */
@Slf4j
@Service
public class MessageService extends MessageServiceGrpc.MessageServiceImplBase {
    private final SessionService sessionService;
    private final TransmittableObjectFactory transmittableObjectFactory;
    private final SchedulingService schedulingService;
    private final Random random = new Random();
    private static final int UPPER_BOUND = 65535;

    public MessageService(SessionService sessionService, TransmittableObjectFactory transmittableObjectFactory,
                          SchedulingService schedulingService) {
        this.sessionService = sessionService;
        this.transmittableObjectFactory = transmittableObjectFactory;
        this.schedulingService = schedulingService;
    }

    /**
     * Given a new connection request, generate a new transmittable object and session for the client. Return a response
     * with the expected number of segments in the response & the session ID
     */
    @Override
    public void connect(ConnectionRequest request, StreamObserver<ConnectionResponse> responseObserver) {
        log.info("Got connection request: {}", request.getClientId());
        var session = this.sessionService.createSession(request.getClientId());

        var numberOfSegments = request.getNumberOfResponseSegments() != 0 ? request.getNumberOfResponseSegments() :
                generateRandomNumberOfResponseSegments();
        var transmittableObject = transmittableObjectFactory.getNewRng(session.getSessionId(), numberOfSegments);

        // Generate Response
        var response = ConnectionResponse.newBuilder()
                .setRemainingSegments(transmittableObject.getNumberOfSegments())
                .setSessionId(session.getSessionId())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();

        log.info("New Session Created with session ID {}", session.getSessionId());
    }

    /**
     * Given a request to start streaming data to the client, we create a Stream IntegerSegmentProducer that will be scheduled to run
     * every second until it's finished streaming
     */
    @Override
    public void stream(StreamRequest request, StreamObserver<Segment> responseObserver) {
        try{
            log.info("Session {} is ready to start receiving data", request.getSessionId());
            var session = sessionService.getSession(request.getSessionId());
            var transmittableObject = transmittableObjectFactory.getTransmittableObject(request.getSessionId());

            // Get the index for the first segment we're going to send
            var startIndex = request.getSegmentIndex();

            // Assert it's a valid index, update the session
            this.checkSegmentInBounds(startIndex, transmittableObject);

            // Create a new Stream IntegerSegmentProducer
            var producer = new IntegerSegmentProducer(transmittableObject, responseObserver, session.getSessionId(), startIndex);

            // Schedule the producer to run every second
            schedulingService.scheduleNewSegmentProducerTask(session.getSessionId(), producer);
            log.info("Task scheduled for session {}", session.getSessionId());
        } catch (NoSuchSessionError | SegmentOutOfBoundsError | NoSuchObjectError e) {
            responseObserver.onError(e);
            responseObserver.onCompleted();
        }
    }

    /**
     * Given a request to Reconnect with a session ID in the request, attempt to resume an existing session if it still
     * exists
     * @param request
     * @param responseObserver
     */
    @Override
    public void reconnect(ReconnectionRequest request, StreamObserver<ConnectionResponse> responseObserver) {
        try {
            var session = this.sessionService.getSession(request.getSessionId());
            var transmittableObject = transmittableObjectFactory.getTransmittableObject(request.getSessionId());
            var lastReceivedSegment = request.getLastReceivedSegment();
            var nextSegment = lastReceivedSegment + 1;

            this.checkSegmentInBounds(nextSegment, transmittableObject);
            sessionService.refreshSession(session.getSessionId());

            var remainingSegments = transmittableObject.getNumberOfSegments() - lastReceivedSegment;
            var response = ConnectionResponse.newBuilder()
                    .setRemainingSegments(remainingSegments)
                    .setSessionId(session.getSessionId())
                    .build();

            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (NoSuchSessionError | SegmentOutOfBoundsError | NoSuchObjectError e) {
            log.error(e.getMessage());
            responseObserver.onError(e);
        }
    }

    /**
     * RPC that allows client to gracefully to end their session.
     * @param request
     * @param responseObserver
     */
    @Override
    public void closeSession(CloseSessionRequest request, StreamObserver<CloseSessionResponse> responseObserver) {
        try {
            var sessionId = request.getSessionId();
            log.info("Got request to close session {}", sessionId);
            this.sessionService.removeSession(sessionId);
            this.schedulingService.endTask(sessionId);
            log.info("Closed session {}", sessionId);
        } catch (NoSuchSessionError e) {
            responseObserver.onError(e);
            responseObserver.onCompleted();
        }
    }

    private void checkSegmentInBounds(int segmentIndex, TransmittableObject object) throws SegmentOutOfBoundsError {
        if (segmentIndex > object.getNumberOfSegments() || segmentIndex == 0){
            throw new SegmentOutOfBoundsError();
        }
    }

    private int generateRandomNumberOfResponseSegments() {
        return random.nextInt(UPPER_BOUND);
    }

}
