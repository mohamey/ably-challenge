package com.yasirmohamed.ably.services.scheduling;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.yasirmohamed.ably.services.sessions.InMemorySessionService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.yasirmohamed.ably.exceptions.NoSuchSessionError;
import com.yasirmohamed.ably.components.SegmentProducer;
import com.yasirmohamed.ably.services.sessions.SessionService;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

/**
 * This service is responsible for running all async tasks in this project. This largely falls under two categories:
 *   - SegmentProducer Tasks - Tasks to write a segment to a response stream every second until completion (or failure)
 *   - Maintenance Tasks - Tasks associated with cleanups - like removing expired sessions from the {@link InMemorySessionService}
 */
@Slf4j
@Service
public class SchedulingService {
    private final SessionService sessionService;
    private final ScheduledExecutorService executorService;
    private final Map<String, RunningTask> runningTasks = Maps.newHashMap();

    public SchedulingService(InMemorySessionService sessionService, ScheduledExecutorService executorService) {
        this.executorService = executorService;
        this.executorService.scheduleAtFixedRate(this::cleanUpTasks, 10, 10, TimeUnit.SECONDS);

        this.sessionService = sessionService;
        scheduleNewMaintenanceTask(sessionService::killExpiredSessions);
    }

    /**
     * Schedule new Segment Producer task to run every second until completion. Stop an existing segment producer task
     * if it exists. Each session can only have one running task.
     */
    public void scheduleNewSegmentProducerTask(String sessionId, SegmentProducer segmentProducer) {
        // If there's already a task scheduled for this session, stop it
        if (this.runningTasks.containsKey(sessionId)) {
            this.stopExistingTask(sessionId);
        }

        var future = this.executorService.scheduleAtFixedRate(segmentProducer.getSendSegmentTask(), 0, 1, TimeUnit.SECONDS);
        this.runningTasks.put(sessionId, new RunningTask(segmentProducer, future));
    }

    /**
     * Schedule new maintenance task to run every 10 seconds
     */
    public void scheduleNewMaintenanceTask(Runnable scheduledTask) {
        this.executorService.scheduleAtFixedRate(scheduledTask, 10, 10, TimeUnit.SECONDS);
    }

    /**
     * Given a session ID, kill it's running task and remove it from our cache
     */
    public void endTask(String sessionId) {
        if (this.runningTasks.containsKey(sessionId)) {
            this.stopExistingTask(sessionId);
            this.runningTasks.remove(sessionId);
        }
    }

    /**
     * Given a sessionId, stop it's associated task
     */
    private void stopExistingTask(String sessionId) {
        log.info("Stopping task for session {}", sessionId);
        this.runningTasks.get(sessionId).getScheduledFuture().cancel(true);
    }

    /**
     * Maintenance task responsible for doing three things:
     *   1. Refreshes the session if it's task is still sending Segments
     *   2. Kills the task if the task is no longer sending segments (due to completion or failure)
     *   3. Stops a running task if the session no longer exists
     */
    public void cleanUpTasks() {
        Set<String> keysToRemove = Sets.newHashSet();
        for (Map.Entry<String, RunningTask> entry: this.runningTasks.entrySet()) {
            var sessionId = entry.getKey();
            var runningTask = entry.getValue();
            try {
                if (runningTask.getTask().isSendingSegments()) {
                    sessionService.refreshSession(sessionId);
                } else {
                    log.info("Task for session {} is finished.", sessionId);
                    keysToRemove.add(sessionId);
                    stopExistingTask(sessionId);
                }
            } catch (NoSuchSessionError e) {
                log.info("Session {} no longer exists, Removing associated tasks", sessionId);
                stopExistingTask(sessionId);
                keysToRemove.add(sessionId);
            }
        }

        keysToRemove.forEach(this.runningTasks::remove);
    }
}

/**
 * Helper class which allows us to associate a task with it's associated ScheduledFuture.
 */
@Getter
@AllArgsConstructor
class RunningTask {
    private SegmentProducer task;
    private ScheduledFuture scheduledFuture;
}
