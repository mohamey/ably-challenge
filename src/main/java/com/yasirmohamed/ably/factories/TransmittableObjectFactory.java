package com.yasirmohamed.ably.factories;

import com.google.common.collect.Maps;
import com.yasirmohamed.ably.exceptions.NoSuchObjectError;
import com.yasirmohamed.ably.models.transmittable.RandomNumberGenerator;
import com.yasirmohamed.ably.models.transmittable.TransmittableObject;
import com.yasirmohamed.ably.services.sessions.SessionService;
import com.yasirmohamed.ably.services.scheduling.SchedulingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Factory responsible for managing transmittable objects in use by sessions. Not particularly necessary for this
 * challenge, but if we were to extend this software to read files from disk - we could share objects that have the
 * file open between sessions and reduce memory usage.
 */
@Slf4j
@Component
public class TransmittableObjectFactory {
    private final SessionService sessionService;
    private final Map<String, TransmittableObject> objectMap;

    public TransmittableObjectFactory(SessionService sessionService, SchedulingService schedulingService) {
        this.sessionService = sessionService;
        this.objectMap = Maps.newHashMap();

        schedulingService.scheduleNewMaintenanceTask(this::deleteExpiredEntries);
    }

    public RandomNumberGenerator getNewRng(String sessionId, int numberOfSegments) {
        var rng = new RandomNumberGenerator(numberOfSegments);
        this.objectMap.put(sessionId, rng);
        return rng;
    }

    public TransmittableObject getTransmittableObject(String sessionId) throws NoSuchObjectError {
        if (!this.objectMap.containsKey(sessionId)) {
            throw new NoSuchObjectError();
        }
        return this.objectMap.get(sessionId);
    }

    public void deleteExpiredEntries() {
        var keysToRemove = this.objectMap.keySet()
                .stream()
                .filter(sessionId -> !this.sessionService.containsSession(sessionId))
                .collect(Collectors.toList());

        keysToRemove.forEach(objectMap::remove);
    }
}
