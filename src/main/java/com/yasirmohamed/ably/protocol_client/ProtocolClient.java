package com.yasirmohamed.ably.protocol_client;

import com.yasirmohamed.ably.proto.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Client class for using the protocol. It's not really production grade, more of just a tool for testing out the
 * protocol.
 */
@Slf4j
public class ProtocolClient {
    private MessageServiceGrpc.MessageServiceStub asyncStub;
    private MessageServiceGrpc.MessageServiceBlockingStub syncStub;
    private StreamObserver<Segment> responseObserver;
    private ManagedChannel channel;
    private String sessionId;
    private final String clientId;
    private final String host;
    private final int port;
    private final AtomicInteger lastReceivedSegment = new AtomicInteger(0);
    private int expectedNumberOfSegments;
    private ResponseBuffer responseBuffer;

    public ProtocolClient(String clientId, String host, int port) {
        this.clientId = clientId;
        this.host = host;
        this.port = port;
    }

    /**
     * Attempt to shutdown the existing channel, open new channel, generate new grpc stubs.
     */
    public void startConnection() {
        this.channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext()
                .build();
        this.asyncStub = MessageServiceGrpc.newStub(channel);
        this.syncStub = MessageServiceGrpc.newBlockingStub(channel);
    }

    public void happyPath(int numSegments) throws InterruptedException {
        this.startConnection();
        this.connect(numSegments);
        this.stream(1);
    }

    public void breakAndResume(int numSegments, int breakIndex) throws InterruptedException {
        this.startConnection();
        this.connect(numSegments);
        this.stream(1, true, breakIndex, 0, true);
    }

    public void breakAndResumeAfterTimeout(int numSegments, int breakIndex, int timeoutSeconds) throws InterruptedException {
        this.startConnection();
        this.connect(numSegments);
        this.stream(1, true, breakIndex, timeoutSeconds, true);
    }

    public void breakAndDontResume(int numSegments, int breakIndex) throws InterruptedException {
        this.startConnection();
        this.connect(numSegments);
        this.stream(1, true, breakIndex, 0, false);
    }

    /**
     * Method to open a connection to the server and initialize a session. We also persist some connection metadata
     * @param numberOfSegments - Number of random numbers the server should generate
     */
    public void connect(int numberOfSegments) {
        var request = ConnectionRequest.newBuilder().setClientId(clientId);
        if (numberOfSegments > 0) {
            request.setNumberOfResponseSegments(numberOfSegments);
        }

        var response = this.syncStub.connect(request.build());
        log.info("Created new Session {}, expecting {} segments", response.getSessionId(), response.getRemainingSegments());

        this.sessionId = response.getSessionId();
        log.info(this.sessionId);
        this.expectedNumberOfSegments = response.getRemainingSegments();
        responseBuffer = new ResponseBuffer(expectedNumberOfSegments);
    }

    /**
     * Method to reconnect to a server given an existing session ID.
     * @param sessionId - Our existing Session ID
     * @param lastReceivedSegmentIndex - Index of the last segment we received
     */
    public void reconnect(String sessionId, int lastReceivedSegmentIndex) {
        try {
            log.info("Reconnecting session {} at index {}", sessionId, lastReceivedSegmentIndex);
            this.syncStub.reconnect(ReconnectionRequest.newBuilder()
                    .setSessionId(sessionId)
                    .setLastReceivedSegment(lastReceivedSegmentIndex)
                    .build()
            );
        } catch (StatusRuntimeException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Overloaded method for streaming from the server without breaking the connection part-way through.
     * @param segmentIndex - Index of the segment to begin streaming from. Uses 1-based indexing
     * @throws InterruptedException when we're unable to wait for the channel to terminate
     */
    public void stream(int segmentIndex) throws InterruptedException {
        this.stream(segmentIndex, false, 0, 0, true);
    }

    /**
     * Method to indicate to the server we are ready to begin streaming data from the server
     * @param segmentIndex - Index of the first segment to start from. Segments use 1-based indexing
     * @param breakEarly - boolean indicating if we should break the stream part-way through and reconnect. This is for
     *                   testing
     * @param breakIndex - Index at which to break the stream
     * @throws InterruptedException when we're unable to wait for the channel to terminate
     */
    public void stream(int segmentIndex, boolean breakEarly, int breakIndex, int timeoutSeconds, boolean resume) throws InterruptedException {
        responseObserver = getSegmentResponseObserver(responseBuffer, breakEarly, breakIndex, timeoutSeconds, resume);
        var request = StreamRequest.newBuilder()
                .setSessionId(this.sessionId)
                .setSegmentIndex(segmentIndex)
                .build();

        this.asyncStub.stream(request, responseObserver);
        this.channel.awaitTermination(expectedNumberOfSegments + 20, TimeUnit.SECONDS);

    }

    /**
     * Method for the client to indicate it's correctly received the file and would like to gracefully shut down the
     * connection
     */
    public void closeSession() {
        log.info("Closing session {}", sessionId);
        this.syncStub.closeSession(CloseSessionRequest.newBuilder()
                .setSessionId(sessionId)
                .build());
        this.responseObserver.onCompleted();
        this.channel.shutdownNow();
    }

    /**
     * Helper method for getting a StreamObserver for handling Segments received by the server. For testing purposes, we
     * allow for the stream to be broken partway through handling the stream
     * @param responseBuffer - Buffer which we will write each of the individual response segments to
     * @param breakEarly - boolean indicating if we should break the stream during transmission. For testing purposes
     * @param breakIndex - Index at which to break the stream.
     * @return - StreamObserver
     */
    private StreamObserver<Segment> getSegmentResponseObserver(ResponseBuffer responseBuffer, boolean breakEarly,
                                                               int breakIndex, int timeoutSeconds, boolean resume) {
        return new StreamObserver<>() {
            @Override
            public void onNext(Segment segmentResponse) {
                var segmentIndex = segmentResponse.getSegmentNumber();
                if (breakEarly && segmentIndex > breakIndex) {
                    try {
                        log.info("Killing connection and waiting {} seconds", timeoutSeconds);
                        terminateConnection();
                        Thread.sleep(timeoutSeconds * 1000L);
                        if (resume) {
                            log.info("Recreating stream at index {}", lastReceivedSegment.get());
                            recreateResponseStream(lastReceivedSegment.get());
                        }
                    } catch (InterruptedException e) {
                        log.info("Unexpected Exception");
                    }
                }

                lastReceivedSegment.getAndSet(segmentIndex);
                log.info("Received Segment {} - Payload: {}", segmentIndex, segmentResponse.getPayload());
                responseBuffer.addSegment(segmentIndex, segmentResponse.getPayload());
                if (lastReceivedSegment.get() == expectedNumberOfSegments) {
                    log.info("Received checksum {}", segmentResponse.getChecksum());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                log.info("Got an error");
                log.info(throwable.getMessage());
            }

            @Override
            public void onCompleted() {
                log.info("Finished!");
                log.info("Checksum is {}", responseBuffer.calculateChecksum());
                closeSession();
            }
        };
    }

    private void terminateConnection() {
        this.channel.shutdownNow();
        this.channel = null;
        this.syncStub = null;
        this.asyncStub = null;
    }

    /**
     * Helper method for breaking the response stream and recreating it using our protocol
     * @param lastReceivedSegmentIndex - Index of the last segment we received before we break the stream. This is a
     *                                 global variable, but is handier during testing to pass the index here.
     * @throws InterruptedException - Refer to the {@link ProtocolClient.stream()} function
     */
    private void recreateResponseStream(int lastReceivedSegmentIndex) throws InterruptedException {
        log.info("Reconnecting session");
        this.startConnection();
        this.reconnect(sessionId, lastReceivedSegmentIndex);
        this.stream(lastReceivedSegmentIndex + 1);
    }
}
