package com.yasirmohamed.ably.protocol_client;

import java.util.zip.CRC32C;
import java.util.zip.Checksum;

/**
 * Helper class for buffering the response from the server. Session independent, we can break the stream as many times
 * as we want and it'll still work.
 */
public class ResponseBuffer {
    private final Integer[] segments;
    private final int expectedNumberOfSegments;

    ResponseBuffer(int expectedNumberOfSegments) {
        this.expectedNumberOfSegments = expectedNumberOfSegments;
        this.segments = new Integer[expectedNumberOfSegments];
    }

    public void addSegment(int segmentIndex, Integer value) {
        var arrayIndex = segmentIndex - 1;
        this.segments[arrayIndex] = value;
    }

    public long calculateChecksum() {
        Checksum crc32 = new CRC32C();
        var byteArray = new byte[this.expectedNumberOfSegments];
        for (int i = 0; i < expectedNumberOfSegments; i++) {
            byteArray[i] = segments[i].byteValue();
        }

        crc32.update(byteArray);
        return crc32.getValue();
    }

}
