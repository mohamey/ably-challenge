package com.yasirmohamed.ably;

import com.yasirmohamed.ably.protocol_client.ProtocolClient;
import com.yasirmohamed.ably.services.messaging.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@Slf4j
@SpringBootApplication
public class AblyApplication implements ApplicationRunner {
    @Autowired
    private MessageService messageService;

    public static void main(String[] args) {
        SpringApplication.run(AblyApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        var runType = getArgument(args, "runType", true);
        if (runType.equals("client")) {
            log.info("Starting client");
            var action = getArgument(args, "action", true);
            var port = Integer.parseInt(getArgument(args, "port", true));

            var segmentsString = getArgument(args, "numSegments", false);
            var numSegments = segmentsString == null ? -1 : Integer.parseInt(segmentsString);

            var breakEarlyString = getArgument(args, "breakEarly", false);
            var breakEarly = breakEarlyString == null ? false : Boolean.parseBoolean(breakEarlyString);

            var breakIndex = -1;
            if (action.equals("breakAndResume") || action.equals("breakAndResumeAfterTimeout") || action.equals("breakAndDontResume")) {
                breakIndex = Integer.parseInt(getArgument(args, "breakIndex", true));
            }

            var timeout = 0;
            if (action.equals("breakAndResumeAfterTimeout")) {
                timeout = Integer.parseInt(getArgument(args, "timeout", true));
            }


            var client = new ProtocolClient("someClient", "localhost", port);

            switch (action) {
                case "happyPath" -> client.happyPath(numSegments);
                case "breakAndResume" -> client.breakAndResume(numSegments, breakIndex);
                case "breakAndDontResume" -> client.breakAndDontResume(numSegments, breakIndex);
                case "breakAndResumeAfterTimeout" -> client.breakAndResumeAfterTimeout(numSegments, breakIndex, timeout);
                default -> log.error("Action not recognized");
            }

        } else if (runType.equals("server")) {
            var port = Integer.parseInt(getArgument(args, "port", true));
            log.info("Starting Server at port {}", port);
            var server = new MessageServer(port, messageService);
            server.start();
        }



    }

    private String getArgument(ApplicationArguments args, String argName, boolean required) throws Exception {
        if (!args.getOptionNames().contains(argName)) {
            if (required) {
                throw new Exception(String.format("%s is a required argument", argName));
            } else {
                return null;
            }
        }

        return args.getOptionValues(argName).get(0);
    }
}
