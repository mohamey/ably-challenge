package com.yasirmohamed.ably.exceptions;

import io.grpc.Status;
import io.grpc.StatusException;

/**
 * Thrown when trying to interact with a session which doesn't exist.
 */
public class NoSuchSessionError extends StatusException {
    public NoSuchSessionError() {
        super(Status.NOT_FOUND);
    }
}
