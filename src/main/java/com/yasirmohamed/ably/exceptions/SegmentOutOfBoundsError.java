package com.yasirmohamed.ably.exceptions;

import io.grpc.Status;
import io.grpc.StatusException;


/**
 * Thrown when trying to access a segment index which doesn't exist
 */
public class SegmentOutOfBoundsError extends StatusException {
    public SegmentOutOfBoundsError() {
        super(Status.OUT_OF_RANGE);
    }
}
