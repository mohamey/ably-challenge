package com.yasirmohamed.ably.exceptions;

import io.grpc.Status;
import io.grpc.StatusException;

/**
 * Thrown when trying to interact with a transmittable object which doesn't exist.
 */
public class NoSuchObjectError extends StatusException {
    public NoSuchObjectError() {
        super(Status.NOT_FOUND);
    }
}