package com.yasirmohamed.ably.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Configuration class for configuring our threadpool for executing async tasks (like sending segments)
 */
@Configuration
public class ExecutorConfig {
    @Bean
    public ScheduledExecutorService executorService() {
        return Executors.newScheduledThreadPool(2);
    }
}