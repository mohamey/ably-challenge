package com.yasirmohamed.ably;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import com.yasirmohamed.ably.services.messaging.MessageService;

import java.io.IOException;

/**
 * Helper class for starting a GRPC Message Server
 */
public class MessageServer {
    private final Server server;

    public MessageServer(int port, MessageService messageService) {
        server = ServerBuilder.forPort(port)
                .addService(messageService)
                .addService(ProtoReflectionService.newInstance())
                .build();
    }

    public void start() throws IOException, InterruptedException {
        this.server.start();
        this.server.awaitTermination();
    }
}
