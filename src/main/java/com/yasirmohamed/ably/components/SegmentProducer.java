package com.yasirmohamed.ably.components;

/**
 * Interface defining a SegmentProducer which allow's it's send segment task to be scheduled by an executor service.
 */
public interface SegmentProducer {
    boolean isSendingSegments();
    Runnable getSendSegmentTask();
}
