package com.yasirmohamed.ably.components;

import com.yasirmohamed.ably.models.transmittable.TransmittableObject;
import com.yasirmohamed.ably.proto.Segment;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class responsible for transmitting Integer segments of a Transmittable object to a given StreamObserver. Since it's
 * a SegmentProducer, it exposes it's sendSegment method via the getSendSegmentTask. This allows the task to be
 * scheduled regularly.
 */
@Slf4j
public class IntegerSegmentProducer implements SegmentProducer {
    private final TransmittableObject<Integer> transmittableObject;
    private final StreamObserver<Segment> responseStream;
    private final String sessionId;
    private int segmentIndex;
    private final AtomicBoolean isActive = new AtomicBoolean(true);

    public IntegerSegmentProducer(TransmittableObject<Integer> transmittableObject,
                                  StreamObserver<Segment> responseStream, String sessionId, int segmentIndex) {
        this.transmittableObject = transmittableObject;
        this.responseStream = responseStream;
        this.sessionId = sessionId;
        this.segmentIndex = segmentIndex;
    }

    @Override
    public boolean isSendingSegments() {
        return this.isActive.get();
    }

    @Override
    public Runnable getSendSegmentTask() {
        return this::sendSegment;
    }

    /**
     * This method will iterate each segment in a transmittable object and write the segment to the responseStream. When
     * sending the final segment, it'll calculate the checksum and send it to the client with the final segment.
     *
     * finally, it'll close the stream after sending all segments.
     */
    private void sendSegment() {
        if (segmentIndex > transmittableObject.getNumberOfSegments()) {
            log.warn("Trying to send segment {} for an object which only has {} segments for session {}",
                    segmentIndex, transmittableObject.getNumberOfSegments(), sessionId);
            return;
        } else if (!isSendingSegments()) {
            log.warn("Task is no longer active and awaiting cleanup");
            return;
        }

        var isFinalSegment = segmentIndex == transmittableObject.getNumberOfSegments();

        // Get a segment
        var payload = transmittableObject.getSegment(segmentIndex);

        // Write it to the stream
        var segmentResponseBuilder = Segment.newBuilder()
                .setSegmentNumber(segmentIndex)
                .setPayload(payload);

        if (isFinalSegment) {
            // This is our final segment, set the checksum
            var checksum = transmittableObject.getChecksum();
            log.info("Finished sending segments for Session {}. Checksum is {}", sessionId,
                    checksum);
            segmentResponseBuilder.setChecksum(checksum);
        }

        try {
            log.info("Sending Segment {}", segmentIndex);
            this.responseStream.onNext(segmentResponseBuilder.build());

            if (isFinalSegment) {
                // Mark the stream complete
                log.info("Finished streaming for session {}", sessionId);
                this.responseStream.onCompleted();
                isActive.getAndSet(false);
            }

            segmentIndex++;
        } catch (Exception e) {
            log.warn("Problem writing to stream for session {} - {}", sessionId, e.getMessage());
            isActive.getAndSet(false);
        }
    }
}
